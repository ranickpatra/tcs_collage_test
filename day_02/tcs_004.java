import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class tcs_004 {
    public static void main(String[] args) {

        // to take input
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String str = "";
        System.out.print("Enter a string: ");
        try {
            str = bufferedReader.readLine();
        } catch (IOException e) {
            System.out.println("IO error");
            return;
        }
        
        str = getReverse(str);
        str = changeCase(str);

        System.out.println(str);

    }

    /* to reverse string */
    private static String getReverse(String str) {
        String reverse = "";

        for(int i=0; i<str.length(); i++) {
            reverse += str.charAt(str.length() - i - 1);
        }

        return reverse;
    }

    private static String changeCase(String str) {
        String changed = "";

        for(int i  = 0; i < str.length(); i++) {
            if(str.charAt(i) < 'a')
                changed += (str.charAt(i)+"").toLowerCase();
            else
                changed += (str.charAt(i)+"").toUpperCase();
        }

        return changed;
    }
}
import java.util.Scanner;

class tcs_005 {


    public static void main(String[] args) {
        
        System.out.println("Enterterm number: ");
        
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int term;
        if(n%2 == 0)
            term = new Prime().getPrime(n/2);
        else
            term = new Fibonaccy().getTerm((n+1)/2);
        
        System.out.println(term);
    }

}

class Fibonaccy {
    public int getTerm(int n) {
        if(n <= 1)
            return n;
        return getTerm(n-1) + getTerm(n-2);

    }
}

class Prime {
    public int getPrime(int n) {
        int i = 0;
        int j = 2;
        while(i < n) {
            if(isPrime(j++))
                i++;
        }
        return (j-1);
    }

    private boolean isPrime(int n) {
        for(int i = 2; i <= n / 2; i++) {
            if(n%i == 0)
                return false;
        }
        return true;
    }

}
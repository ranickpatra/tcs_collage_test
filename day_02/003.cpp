#include <iostream>
#include <string>

using namespace std;

string encode(string str)
{
    int count = 0;
    char current_char = str[0];
    string encoded_str = "";
    for (int i = 0; i < str.length()+1; i++)
    {
        if (current_char == str[i])
        {
            count++;
        }
        else
        {
            encoded_str += current_char + (current_char >= 'a' ? -32 : 32);
            if (count > 1)
                encoded_str += count+'0';
            current_char = str[i];
            count = 1;
        }
        
    }
    

    return encoded_str;
}

int main()
{
    cout << "Enter a string: ";
    string str;
    cin >> str;
    cout << encode(str) << endl;
    return 0;
}
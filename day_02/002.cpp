#include <iostream>

using namespace std;

int get__nth_term(int n)
{
    int cterm_0, cterm_1, cterm_2;
    for(int i = 0; i < n; i++) {
        cterm_2 = cterm_1;
        cterm_1 = cterm_0;
        if(i < 2) {
            cterm_0 = 0;
        } else {
            if(i%2 == 0) {
                cterm_0 = cterm_2 + 2;
            } else {
                cterm_0 /= 2;
            }
        }
        // to print n the term during iteration
        // cout << cterm_0 << endl;
    }

    return cterm_0;
}


int main()
{

    cout << "Enter term number: ";
    int n;
    cin >> n;
    cout << n << "term is: " << get__nth_term(n) << endl;
    return 0;
}
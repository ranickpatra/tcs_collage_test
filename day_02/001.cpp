#include <iostream>

using namespace std;

int get__nth_term(int n)
{
    int ctrm_0, ctrm_1, ctrm_2;
    for (int i = 0; i < n; i++)
    {
        // chnaging the term position
        ctrm_2 = ctrm_1;
        ctrm_1 = ctrm_0;
        if (i < 2)
        {
            ctrm_0 = 0;
        }
        else
        {
            ctrm_0 = ctrm_2 + (i % 2 ? 6: 7);
        }
        // to print term
        //cout << ctrm_0 << endl;
        
    }

    return ctrm_0;
}

int main()
{
    cout << "Give term number: ";
    int n;
    cin >> n;
    cout << get__nth_term(n) << endl;
    return 0;
}
